from typing import Any
from django import forms
from .models import Project
from django.contrib.auth import get_user_model

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        # fields = '__all__'
        exclude = ['users']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Название проекта'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            # 'users': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

class UserAddForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['users']
        widgets = {
            'users': forms.SelectMultiple(attrs={'class': 'form-select'})
        }
    
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user_obj', None)
        super(UserAddForm, self).__init__(*args, **kwargs)
        if user:
            self.fields['users'].queryset = get_user_model().objects.exclude(pk=user.pk)

