from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse


class Project(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    users = models.ManyToManyField(get_user_model(), related_name='projects', verbose_name='Пользователь')

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('project_detail', kwargs={
            'pk': self.pk
        })
    class Meta:
        permissions = [
            ('add_user_to_project', 'Can add user to project')
        ]