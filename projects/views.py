from django.forms import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from .models import Project
from .forms import ProjectForm, UserAddForm
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import PermissionRequiredMixin

class ProjectView(ListView):
    template_name = 'index.html'
    model = Project
    context_object_name = 'projects'
    paginate_by = 5
    paginate_orphans = 1

class ProjectDetailView(DetailView):
    template_name = 'projects/project_detail.html'
    model = Project
    context_object_name = 'project'

class ProjectCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'projects/project_create.html'
    model = Project
    form_class = ProjectForm
    permission_required = 'projects.add_project'

class ProjectUpdateView(PermissionRequiredMixin, UpdateView):
    template_name = 'projects/project_update.html'
    model = Project
    form_class = ProjectForm
    permission_required = 'projects.change_project'

class ProjectDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'projects/project_delete.html'
    model = Project
    context_object_name = 'project'
    success_url = reverse_lazy('home_page')
    permission_required = 'projects.delete_project'

class UserAddToProjectView(PermissionRequiredMixin, UpdateView):
    model = Project
    form_class = UserAddForm
    template_name = 'projects/user_add.html'
    context_object_name = 'project'
    permission_required = 'projects.add_user_to_project'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user_obj'] = self.request.user
        return kwargs
    
    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        project = form.save()
        user = self.request.user
        if user not in project.users.all():
            project.users.add(user)
        return redirect(self.get_success_url())
    
    def get_success_url(self) -> str:
        return reverse('project_detail', kwargs={'pk': self.object.pk})
