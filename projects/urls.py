from django.urls import path, include
from projects.views import ProjectDetailView, ProjectCreateView, ProjectUpdateView, ProjectDeleteView, UserAddToProjectView


urlpatterns = [
    path('detail/<pk>/', ProjectDetailView.as_view(), name='project_detail'),
    path('create/', ProjectCreateView.as_view(), name='project_create'),
    path('update/<pk>', ProjectUpdateView.as_view(), name='project_update'),
    path('delete/<pk>',ProjectDeleteView.as_view(), name='project_delete'),
    path('<pk>/add_user/', UserAddToProjectView.as_view(), name='add_user')
]