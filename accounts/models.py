from django.db import models
from django.contrib.auth import get_user_model

class Profile(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name = 'profile',
        on_delete = models.CASCADE,
        verbose_name = 'Пользователь' 
        )
    position = models.CharField(max_length=200, null=False, blank=False, verbose_name='Должность')
    phone = models.CharField(max_length=20, null=True, blank=True, verbose_name='Телефон')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Аватар')

    def __str__(self) -> str:
        return self.user.get_full_name()