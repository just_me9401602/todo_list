from django.urls import path
from .views import RegisterView, UserDetailView, LoginView, LogoutView, UserEditView, UserPasswordChangeView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/', UserDetailView.as_view(), name='user_detail'),
    path('profile_edit/', UserEditView.as_view(), name='user_edit'),
    path('password_change/', UserPasswordChangeView.as_view(), name='password_change')
]