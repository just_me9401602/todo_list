from typing import Any
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from .models import Profile

class MyUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
        labels = {
            'username': 'Логин',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'email': 'Email'
        }

        widgets = {
            'username': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'first_name': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'last_name': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'email': forms.EmailInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
        }
    
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        for field_name in ['password1', 'password2']:
            self.fields[field_name].widget.attrs.update({'class': 'form-control'})
        self.fields['password1'].label='Пароль'
        self.fields['password2'].label='Подтверждение пароля'

class UserEditForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email']
        labels = {
            'first_name' : 'Имя',
            'last_name': 'Фамилия',
            'email': 'Email'
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'last_name': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'email': forms.EmailInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
        }

class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['position', 'phone', 'avatar']
        labels = {
            'position': 'Должность',
            'phone': 'Телефон',
            'avatar': 'Аватар'
        }
        widgets = {
            'position': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'phone': forms.TextInput(attrs={'class':'form-control', 'autocomplete': 'off'}),
            'avatar': forms.FileInput(attrs={'class':'form-control'})
        }

class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(label='Новый пароль', strip=False, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password_confirm = forms.CharField(label='Подтверждение пароля', strip=False, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    old_password = forms.CharField(label='Старый пароль', strip=False, widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Пароли не совпадают')
        return password_confirm
    
    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        print(self.instance.check_password(old_password))
        if not self.instance.check_password(old_password):
            raise forms.ValidationError('Неверный пароль')
        return old_password
    
    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data.get('password'))
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ['old_password', 'password', 'password_confirm']         