from django import forms
from django.db.models.base import Model
from django.forms import widgets
from django.forms.utils import ErrorList
from .models import Task, Status
from django.contrib.auth import get_user_model
from projects.models import Project

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description', 'status', 'users']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Название задачи'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'status': forms.Select(attrs={'class': 'form-control'}),
            'users': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user_obj', None)
        project = kwargs.pop('project', None)
        super(TaskForm, self).__init__(*args, **kwargs)
        if user:
            self.fields['users'].queryset = project.users.exclude(pk=user.pk)
            

class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')