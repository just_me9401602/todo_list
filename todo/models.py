from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
from projects.models import Project

class Status(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name='Название')

    def __str__(self):
        return self.name

class Task(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Описание')
    status = models.ForeignKey(to=Status, on_delete=models.CASCADE, null=False, related_name='tasks', verbose_name='Статус')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    users = models.ManyToManyField(get_user_model(), related_name='tasks', blank=True, verbose_name='Пользователь')
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE, null=True, blank=True, related_name='tasks', verbose_name='Проект')
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('task_detail', kwargs={
            'pk': self.pk
        })


# Create your models here.
