from django.urls import path
from todo.views import (TaskStatusFilter, TaskDetailView, TaskCreateView,
                        TaskUpdateView, TaskDeleteView, TaskView)


urlpatterns = [
    path('list/', TaskView.as_view(), name='task_list'),
    path('status/<s_pk>/date/<filter>/user/<user_pk>/', TaskStatusFilter.as_view(), name='task_filter'),
    path('detail/<pk>/', TaskDetailView.as_view(), name='task_detail'),
    path('create/', TaskCreateView.as_view(), name='task_create'),
    path('update/<pk>/', TaskUpdateView.as_view(), name='task_update'),
    path('delete/<pk>/', TaskDeleteView.as_view(), name='task_delete'),
]
