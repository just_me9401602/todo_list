from typing import Any
from django.db.models.query import QuerySet
from django.forms import BaseModelForm
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.views.generic.base import ContextMixin
from .models import Task, Status
from .forms import TaskForm, SearchForm
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from django.utils.http import urlencode
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth import get_user_model
from projects.models import Project
from django.contrib.auth.models import User

class GetProjectMixin(ContextMixin):
    def get_project(self):
        pk = self.kwargs['p_pk']
        project = Project.objects.get(pk=pk)
        return project
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['project'] = self.get_project()
        return context


class TaskView(GetProjectMixin, ListView):
    template_name = 'tasks/task_list.html'
    model = Task
    context_object_name = 'tasks'
    paginate_by = 5
    paginate_orphans = 1
    form = SearchForm

    def filter_is(self):
        return 'decrease'    

    def get_queryset(self) -> QuerySet[Any]:
        queryset = self.get_project().tasks.all().order_by('-created_at')
        if self.search_value:
            query = Q(title__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset
    
    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_status_list(self, current_user):
        status_list = []
        if current_user == 'all':
            for status in Status.objects.all():
                status_list.append({'pk': status.id,'name': status.name, 'tasks': Status.objects.filter(tasks__project=self.get_project(), name=status.name).count()})
        else:
            for status in Status.objects.all():
                status_list.append({'pk': status.id,'name': status.name, 'tasks': Status.objects.filter(tasks__project=self.get_project(), name=status.name, tasks__users=current_user).count()})
        return status_list

    def get_user_list(self):
        user_list = []
        for user in self.get_project().users.all():
            user_list.append({'pk': user.pk, 'username': user.username, 'tasks': Task.objects.filter(project=self.get_project(), users=user).count()})
        return user_list
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        # context['project'] = self.get_project()
        context['statuses'] = self.get_status_list(current_user='all')
        context['current_status_pk'] = 'all'
        context['current_user_pk'] = 'all'
        total_tasks = self.get_paginator(self.get_project().tasks.all(), per_page=5).count
        context['total_tasks'] = total_tasks
        context['user_tasks'] = total_tasks
        context['current_filter'] = self.filter_is()
        context['form'] = self.form
        context['users'] = self.get_user_list()
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context


class TaskStatusFilter(TaskView):
    def get_status_pk(self):
        pk = self.kwargs['s_pk']
        return pk
    
    def get_user_pk(self):
        pk = self.kwargs['user_pk']
        return pk
    
    def filter_is(self):
        filter = self.kwargs['filter']
        return filter
    
    def get_queryset(self) -> QuerySet[Any]:
        queryset = super().get_queryset()
        if self.get_user_pk()!='all':
            queryset = Task.objects.filter(users=self.get_user_pk(), project=self.get_project())
        if self.get_status_pk()!='all': 
            queryset = queryset.filter(status__id=self.get_status_pk())
            # print(f'status_pk: {self.get_user_pk()}, project: {self.get_project()}')
        if self.filter_is() == 'increase':
            queryset = queryset.order_by('created_at')
        elif self.filter_is() == 'decrease':
            queryset = queryset.order_by('-created_at')
        return queryset
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        if self.get_status_pk()!='all':
            context['current_status_pk'] = int(self.get_status_pk())
        if self.get_user_pk()!='all':
            context['current_user_pk'] = int(self.get_user_pk())
            total_tasks = self.get_paginator(self.get_project().tasks.filter(users__id=self.get_user_pk()), per_page=5).count
            context['user_tasks'] = total_tasks
            context['statuses'] = self.get_status_list(current_user=self.get_user_pk())
        return context
    

class TaskCreateView(PermissionRequiredMixin, GetProjectMixin, CreateView):
    template_name = 'tasks/task_create.html'
    model = Task
    form_class = TaskForm
    permission_required = 'todo.add_task'

    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['user_obj'] = self.request.user
        kwargs['project'] = self.get_project()
        return kwargs
    
    # def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
    #     context = super().get_context_data(**kwargs)
    #     context['project'] = self.get_project()
    #     return context
    
    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        form.instance.project = self.get_project()
        self.object = form.save()
        self.object.users.add(self.request.user)
        return redirect(self.get_success_url())
    
    def get_success_url(self):
        return reverse('task_detail', kwargs={'p_pk': self.get_project().pk,'pk': self.object.pk})


class TaskDetailView(GetProjectMixin, DetailView):
    template_name = 'tasks/task_detail.html'
    model = Task
    context_object_name = 'task'


class TaskUpdateView(PermissionRequiredMixin, GetProjectMixin, UpdateView):
    template_name = 'tasks/task_update.html'
    model = Task
    context_object_name = 'task'
    form_class = TaskForm
    permission_required = 'todo.change_task'
    
    def get_form_kwargs(self) -> dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['user_obj'] = self.request.user
        kwargs['project'] = self.get_project()
        return kwargs
    
    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        self.object = form.save()
        self.object.users.add(self.request.user)
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('task_detail', kwargs={'p_pk': self.object.project.pk,'pk': self.object.pk})


class TaskDeleteView(PermissionRequiredMixin, DeleteView):
    template_name ='tasks/task_delete.html'
    model = Task
    context_object_name = 'task'
    success_url = reverse_lazy('home_page')
    permission_required = 'todo.delete_task'
    
    def get_success_url(self):
        return reverse('task_list', kwargs={'p_pk': self.object.project.pk})

